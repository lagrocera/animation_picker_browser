//
//  PickerViewController.h
//  Animation_Picker_Browser
//
//  Created by Marlon David Ruiz Arroyave on 19/10/14.
//  Copyright (c) 2014 Marlon David Ruiz Arroyave. All rights reserved.
//

#import "ViewController.h"

@interface PickerViewController : ViewController <UIPickerViewDelegate, UIPickerViewDataSource>


@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIPickerView *countryPicker;

@end
