//
//  AppDelegate.h
//  Animation_Picker_Browser
//
//  Created by Marlon David Ruiz Arroyave on 19/10/14.
//  Copyright (c) 2014 Marlon David Ruiz Arroyave. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

