//
//  BrowserViewController.m
//  Animation_Picker_Browser
//
//  Created by Marlon David Ruiz Arroyave on 20/10/14.
//  Copyright (c) 2014 Marlon David Ruiz Arroyave. All rights reserved.
//

#import "BrowserViewController.h"

@interface BrowserViewController ()

@end

@implementation BrowserViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)html:(id)sender {
    NSString *html = @"<h1>Hola desde HTML</h1><p style='color:red'>Esto es un Parrafo de texto</p>";
    [_browser loadHTMLString:html baseURL:nil];
}

- (IBAction)js:(id)sender {
    NSString *script = @"alert('Hola desde js')";
    [_browser stringByEvaluatingJavaScriptFromString:script];
}

- (IBAction)web:(id)sender {
    NSURL *url = [[NSURL alloc]initWithString:@"http://apple.com"];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url];
    [_browser setScalesPageToFit:NO];
    [_browser loadRequest:request];
}

- (IBAction)pdf:(id)sender {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"informe" ofType:@"pdf"];
    NSData *data = [[NSData alloc]initWithContentsOfFile:path];
    [_browser setScalesPageToFit:YES];
    [_browser loadData:data MIMEType:@"application/pdf" textEncodingName:nil baseURL:nil];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [_activityIndicator startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [_activityIndicator stopAnimating];
}



@end
