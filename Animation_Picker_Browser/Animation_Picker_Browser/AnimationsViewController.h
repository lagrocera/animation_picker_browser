//
//  AnimationsViewController.h
//  Animation_Picker_Browser
//
//  Created by Marlon David Ruiz Arroyave on 20/10/14.
//  Copyright (c) 2014 Marlon David Ruiz Arroyave. All rights reserved.
//

#import "ViewController.h"

@interface AnimationsViewController : ViewController
@property (weak, nonatomic) IBOutlet UIView *viewOne;
@property (weak, nonatomic) IBOutlet UIView *viewTwo;

- (IBAction)stopAnimation:(id)sender;
- (IBAction)simpleAnimation:(id)sender;
- (IBAction)callBackAnimation:(id)sender;
- (IBAction)OptionAnimation:(id)sender;
@end
