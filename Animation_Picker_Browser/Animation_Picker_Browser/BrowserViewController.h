//
//  BrowserViewController.h
//  Animation_Picker_Browser
//
//  Created by Marlon David Ruiz Arroyave on 20/10/14.
//  Copyright (c) 2014 Marlon David Ruiz Arroyave. All rights reserved.
//

#import "ViewController.h"

@interface BrowserViewController : ViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *browser;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (IBAction)html:(id)sender;
- (IBAction)js:(id)sender;
- (IBAction)web:(id)sender;
- (IBAction)pdf:(id)sender;

@end
