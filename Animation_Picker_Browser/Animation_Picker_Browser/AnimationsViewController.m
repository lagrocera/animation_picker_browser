//
//  AnimationsViewController.m
//  Animation_Picker_Browser
//
//  Created by Marlon David Ruiz Arroyave on 20/10/14.
//  Copyright (c) 2014 Marlon David Ruiz Arroyave. All rights reserved.
//

#import "AnimationsViewController.h"

@interface AnimationsViewController ()

@end

@implementation AnimationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)stopAnimation:(id)sender {
    [_viewTwo.layer removeAllAnimations];
}

- (IBAction)simpleAnimation:(id)sender {
    if (((UISwitch *)sender).on) {
        [UIView animateWithDuration:1.0 animations:^{
            _viewOne.frame = CGRectMake(_viewOne.frame.origin.x, _viewOne.frame.origin.y, _viewOne.frame.size.width, 110);
            _viewOne.alpha = 0;
        }];
    }else{
        [UIView animateWithDuration:1.0 animations:^{
            _viewOne.frame = CGRectMake(_viewOne.frame.origin.x, _viewOne.frame.origin.y, _viewOne.frame.size.width, 0);
            _viewOne.alpha = 1;
        }];
    }
}

- (IBAction)callBackAnimation:(id)sender {
    [UIView animateWithDuration:1.0 animations:^{
        _viewTwo.backgroundColor = [UIColor grayColor];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 animations:^{
            _viewTwo.alpha = 0;
        }];
    }];
}

- (IBAction)OptionAnimation:(id)sender {
    [UIView animateWithDuration:1.0 delay:1 options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat animations:^{
        _viewTwo.alpha = 0;
    } completion:nil];
}
@end
